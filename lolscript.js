/**
 * Created with JetBrains WebStorm.
 * User: 박상만
 * Date: 16. 4. 13
 * Time: 오전 3:12
 * To change this template use File | Settings | File Templates.
 */



$( document ).bind( 'mobileinit', function(){
    $.mobile.loader.prototype.options.text = "loading";
    $.mobile.loader.prototype.options.textVisible = false;
    $.mobile.loader.prototype.options.theme = "b";
    $.mobile.loader.prototype.options.html = "";

    $.mobile.loading( 'show', {
        text: 'loading',
        textVisible: true,
        theme: 'b',
        html: ""
    });

    console.log('ddd');
});

var memberList = [  {id:1287476,name:"상만"} ,{id:3663671,name:"동주"},{id:19484480,name:"인철"},{id:11475495,name:"자한"},{id:2033447,name:"상현"},{id:35390667,name:"건우"},{id:3491434,name:"종석"}];
var commonGameID = [];
var matchInfo = { seoul:null, changwon:null };

var matchGameInf0_s = [];
var matchGameInf0_c = [];

var seoulWin = 0;
var changWonWin = 0;
var champCache=[];

var cahmpStats={};

function playerInfo(playID,chmpID){

    result ={name:playID,champName:null,key:null}
    for(idx4=0 ; idx4<memberList.length;idx4++){
        var memberInfo = memberList[idx4];
        if(memberInfo.id == playID ){
            result.name = memberInfo.name
        }
    }

    var isCache=false;

    for(idx=0 ; idx<champCache.length;idx++){
        var champInfo = champCache[idx];
        if(champInfo.id == chmpID ){
            result.champName = champInfo.name;
            result.key=champInfo.key;
            isCache = true;
        }
    }

    if(isCache==false){
        var requestUrl = "https://global.api.pvp.net/api/lol/static-data/kr/v1.2/champion/" + chmpID +"?api_key=11896677-f0e9-408e-a625-5d43e4e817fb"
        $.ajax({
            url: requestUrl,
            async: false,
            success: function( data ) {
                champCache.push(data);
                result.champName = data.name;
                result.key = data.key;
            }
        });
    }

    return result;
}


var createItem = function(title,winTeam,loseTeam){
    $("#gameInfoItem").append('<li data-role="collapsible" data-iconpos="right" data-inset="false">' + title + winTeam + loseTeam +'</li>');
    //$("#gameInfoItem").trigger( "refresh" );

    //$('#gameInfoItem').listview('refresh');
    $('#gameInfoItem').trigger("create");
}

//champStats
var createChampStat = function(){
    for (var key in cahmpStats) {
        //console.log(key + ' => ' + p[key]);

        var rate = cahmpStats[key].w / (cahmpStats[key].w + cahmpStats[key].l) * 100;
        $("#champui").append('<li>' + cahmpStats[key].img  + key + ' W:' + cahmpStats[key].w + '/L:' + cahmpStats[key].l +  ' 승률:' + rate +  '</li>');

        // key is key
        // value is p[key]
    }
    $('#champui').trigger("create");

}

var createMos = function(){
    //https://kr.api.pvp.net/api/lol/kr/v2.2/match/2384386691?api_key=11896677-f0e9-408e-a625-5d43e4e817fb
}



var runChangAPI = function(){
    //창원팀조회
    $.ajax({
        url: "https://kr.api.pvp.net/api/lol/kr/v1.3/game/by-summoner/11475495/recent?api_key=11896677-f0e9-408e-a625-5d43e4e817fb",
        success: function( data ) {
            matchInfo.chanwon = data.games;
            for (i = 0; i < data.games.length ; i++) {
                var gameInfo = data.games[i];

                var privateStats={};

                if(gameInfo.gameType == "CUSTOM_GAME"){

                    $.ajax({
                        url: "https://kr.api.pvp.net/api/lol/kr/v2.2/match/"+gameInfo.gameId+"?api_key=11896677-f0e9-408e-a625-5d43e4e817fb",
                        async: false,
                        success: function( data ) {
                            //participants
                            for( idx9=0 ; idx9 < data.participants.length; idx9++){
                                var curPans = data.participants[idx9];
                                var panKey = curPans.teamId + "-" + curPans.championId;
                                privateStats[panKey]=curPans;
                                //console.log(curPans);
                            }
                        }
                    });

                    matchInfo.changwon = gameInfo;
                    for( idx2 = 0 ; idx2 < commonGameID.length ; idx2++ ){
                        var curGameId = commonGameID[idx2];
                        var wintitle=""
                        var membersC = '<ul data-role="listview" data-theme="a">';
                        var membersS = '<ul data-role="listview" data-theme="b">';

                        var gameDate = new Date(0);
                        gameDate.setUTCSeconds(gameInfo.createDate/1000);

                        //matchCreation
                        var statInfo = gameDate.getMonth()+1 + "/"  + gameDate.getDay() +    " 플레이시간:" + Math.floor(gameInfo.stats.timePlayed/60) +"분";

                        if(gameInfo.gameId == curGameId){
                            if(gameInfo.stats.win==true){
                                changWonWin++;
                                wintitle = "<h2>창원레드팀(승) " +statInfo +"</h2>";
                            }else{
                                seoulWin++;
                                wintitle = "<h2>서울블루팀(승) " +statInfo +"</h2>";

                            }

                            var meInfo = playerInfo(11475495,gameInfo.championId);

                            var panKey = gameInfo.teamId + "-" + gameInfo.championId;
                            var meStats = privateStats[panKey].stats;
                            //kills        // deaths  //largestMultiKill
                            var killInfo = "킬:" + meStats.kills + " 데쓰:" + meStats.deaths + " 연속킬:" + meStats.largestMultiKill;

                            var imgEle ='<img src="http://ddragon.leagueoflegends.com/cdn/6.7.1/img/champion/'+ meInfo.key + '.png" />';
                            var imgEle2 ='<img width="20" height="20" src="http://ddragon.leagueoflegends.com/cdn/6.7.1/img/champion/'+ meInfo.key + '.png" />';
                            membersC = membersC + "<li>" + imgEle +  "(" + meInfo.name  + ") " + killInfo + " </li>";

                            if(cahmpStats[meInfo.name+meInfo.champName] == undefined ){
                                cahmpStats[meInfo.name+meInfo.champName] = {w:0,l:0,img:imgEle2};

                                if(gameInfo.stats.win==true)
                                    cahmpStats[meInfo.name+meInfo.champName].w++;
                                else
                                    cahmpStats[meInfo.name+meInfo.champName].l++;

                            }

                            for( idx3 =0 ; idx3 < gameInfo.fellowPlayers.length ; idx3++){
                                var curMember = gameInfo.fellowPlayers[idx3];
                                var pInfo = playerInfo(curMember.summonerId,curMember.championId);

                                var panKey = curMember.teamId + "-" + curMember.championId;
                                var meStats = privateStats[panKey].stats;
                                //kills        // deaths  //largestMultiKill
                                var killInfo = "킬:" + meStats.kills + " 데쓰:" + meStats.deaths + " 연속킬:" + meStats.largestMultiKill;

                                var imgEle ='<img src="http://ddragon.leagueoflegends.com/cdn/6.7.1/img/champion/'+ pInfo.key + '.png" />';
                                var imgEle2 ='<img width="20" height="20" src="http://ddragon.leagueoflegends.com/cdn/6.7.1/img/champion/'+ pInfo.key + '.png" />';

                                if(cahmpStats[pInfo.name+pInfo.champName] == undefined ){
                                    cahmpStats[pInfo.name+pInfo.champName] = {w:0,l:0,img:imgEle2};

                                }

                                //membersC = membersC + "<li>" + imgEle +  "(" + meInfo.name  + ") " + killInfo + " </li>";

                                //우리팀
                                if(curMember.teamId == gameInfo.teamId){
                                    membersC = membersC + "<li>" +  imgEle + "(" + pInfo.name  + ") " + killInfo + "</li>";
                                    if(gameInfo.stats.win==true)
                                        cahmpStats[pInfo.name+pInfo.champName].w++;
                                    else
                                        cahmpStats[pInfo.name+pInfo.champName].l++;

                                }else{
                                    membersS = membersS +  "<li>" +  imgEle + "(" + pInfo.name  + ") " + killInfo + "</li>";
                                    if(gameInfo.stats.win==true)
                                        cahmpStats[pInfo.name+pInfo.champName].l++;
                                    else
                                        cahmpStats[pInfo.name+pInfo.champName].w++;
                                }
                            }

                            membersC=membersC+"</ul>";
                            membersS=membersS+"</ul>";

                            if(gameInfo.stats.win==true)
                                createItem(wintitle,membersC,membersS);
                            else
                                createItem(wintitle,membersS,membersC);

                            //$("#gameItem").append('<li><span class="ui-icon ui-icon-arrowthick-2-n-s">' + membersS +"vs" +membersC + '</span></li>');

                        }
                    }
                }
            }
            var score  = "서울:" + seoulWin + " vs 창원:" + changWonWin;
            $( "#teamscore" ).html( "<h3>" + score + "</h3>" );
            createChampStat();

            $.mobile.loading( 'hide', {
                text: 'loading',
                textVisible: true,
                theme: 'b',
                html: ""
            });
        }
    });
}


//서울팀 조회
$.ajax({
    url: "https://kr.api.pvp.net/api/lol/kr/v1.3/game/by-summoner/1287476/recent?api_key=11896677-f0e9-408e-a625-5d43e4e817fb",
    success: function( data ) {

        $.mobile.loading( 'show', {
            text: 'loading',
            textVisible: true,
            theme: 'b',
            html: ""
        });

        matchInfo.seoul = data.games;
        for (i = 0; i < data.games.length ; i++) {
            var gameInfo = data.games[i];
            if(gameInfo.gameType == "CUSTOM_GAME"){
                commonGameID.push(gameInfo.gameId);
                matchGameInf0_s[gameInfo.gameId] = gameInfo;
            }
        }
        runChangAPI();
    }
});